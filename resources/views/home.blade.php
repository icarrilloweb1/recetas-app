@extends('layouts.homelayout')

@section('content')
<div class="container">

    @foreach ($categories as $category)
      @if (count($category->recipes))       
        <div class="recetas-recomendadas contenedor">

          <a href="category/{{ $category->slug }}" class="contenedor-titulo-controles link_category">
            <h3>Categoria: {{ $category->titulo }}</h3>          
          </a>        

          <div class="contenedor-principal">
            
            <button role="button" id="flecha-izquierda" class="flecha-izquierda" onclick="izquierda({{ $category->id }})"><i class="fas fa-angle-left"></i></button>
            
            <div class="contenedor-carousel contenedor-carrousel-{{ $category->id }}" id="contenedor-carrousel-{{ $category->id }}">
              
              <div class="carousel">
                @foreach ($category->recipes as $recipe)
                  <div class="receta">
                    <a href="recipe/{{ $recipe->slug }}">
                      <img src="/storage/{{ $recipe->imagen }}" alt="Imagen Receta">
                      <span class="recetas-titulo">{{ $recipe->titulo}}<br><small>{{ \Carbon\Carbon::parse($recipe->fecha_publicacion)->format('d-m-Y') }}</small></span>
                    </a>
                  </div>
                @endforeach                            
              </div>

            </div>

            <button role="button" id="flecha-derecha" class="flecha-derecha" onclick="derecha({{ $category->id }})"><i class="fas fa-angle-right"></i></button>
          
          </div>
        </div>
      @endif
    @endforeach 
	
</div>
@endsection

<script>    
  function derecha(valor) {
    let filaseleccionada = document.querySelector('.contenedor-carrousel-'+valor);
	  filaseleccionada.scrollLeft += filaseleccionada.offsetWidth;
  }

  function izquierda(valor) {
    let filaseleccionada = document.querySelector('.contenedor-carrousel-'+valor);
	  filaseleccionada.scrollLeft -= filaseleccionada.offsetWidth;
  }
</script>