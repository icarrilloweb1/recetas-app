@extends('layouts.homelayout')

@section('content')
<div class="container">
  
    <div class="titulo_categoria">
        
        <a href="{{ route('home') }}"><i class="fas fa-chevron-left"></i> volver al Home</a>
    </div>

    <div class="ficha_receta">
        <img src="/storage/{{ $recipe->imagen }}" alt="Imagen Receta" />
        <div class="detalle_receta">
            <h3>Receta: <b>{{ $recipe->titulo }}</b></h3>
            <span class="propiedad_receta">Categoría:</span> <a href="/category/{{ $recipe->category->slug }}" class="data_receta">{{ $recipe->category->titulo }}</a><br><br>
            <span class="propiedad_receta">Tiempo de preparación:</span> <span class="data_receta">{{ $recipe->tiempo_preparacion }} mins</span><br>
            <span class="propiedad_receta">Número de raciones:</span> <span class="data_receta">{{ $recipe->numero_raciones }}</span><br><br>
            <span class="propiedad_receta">Ingredientes:</span> <span class="data_receta">{!! $recipe->ingredientes !!}</span><br><br>
            <span class="propiedad_receta">Procedimiento:</span> <span class="data_receta">{!! $recipe->procedimiento !!}</span><br><br>
            <span class="propiedad_receta">Fecha de publicación:</span> <span class="data_receta">{{ \Carbon\Carbon::parse($recipe->fecha_publicacion)->format('d-m-Y') }}</span>
            

        </div>
    </div>
	
</div>
@endsection