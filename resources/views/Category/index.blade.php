@extends('layouts.dataTables')

@section('content')
<div class="backoffice-div">
    
    <div class="row">
        <div class="col-12 table-responsive">
        <h3 align="center">Listado de Categorias</h3>
        <div align="left">
            <a href="{{ route('category.create') }}" class="btn btn-success">Añadir categoría</a>
        </div>
        <br />
            <table id="categories_datatable" class="table table-striped table-bordered categories_datatable"> 
                <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Título</th>
                        <th>Slug</th>
                        <th>Activa</th>
                        <th width="300px">Opciones</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div> 
</div>

<script :is="'script'"> 
$(function () {
    
    var table = $('.categories_datatable').DataTable({
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            processing: true,
            serverSide: true,
            ajax: "{{ route('category.index') }}",
            columns: [
                {data: 'id', name: 'Id'},
                {data: 'titulo', name: 'titulo'},
                {data: 'slug', name: 'slug'},
                {data: 'active', name: 'active'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
    });    
    
    $(document).on("click", ".delete", function (event) {
        event.preventDefault();
        $textoconfirm = $(this).attr("textoconfirm");
                	
        if (confirm($textoconfirm)) {
            let category_id = $(this).attr('id');
            $url = "/admin/categories/delete/"+category_id;
            $.ajax($url, {
                "type": "GET",
                "success": function (data) {
                    $('.categories_datatable').DataTable().ajax.reload();    
                },
                "error": function (data) {
                },
                "async": false
            });
        }            
    });

    $(document).on("click", ".activebtn", function (event) {
        event.preventDefault();
        let category_id = $(this).attr('id');
            $url = "/admin/categories/"+category_id+"/active";
            $.ajax($url, {
                "type": "GET",
                "success": function (data) {
                    $('.categories_datatable').DataTable().ajax.reload();    
                },
                "error": function (data) {
                },
                "async": false
            });                    
    });

    $(document).on("click", ".edit", function (event) {
        event.preventDefault();
        let slug = $(this).attr('slug');
            $url = "/admin/categories/"+slug+"/editar";
            window.location.replace($url);                 
    });

}); 
</script>  

@endsection