@extends('layouts.app')

@section('content')
<div class="backoffice-div">
    
    <div class="link_back">
        <a href="{{ route('category.index') }}"><i class="fas fa-chevron-left"></i> volver al listado de categorias</a>
    </div>
    <div class="card card_formulario" >
        <div class="card-body">
          <h5 class="card-title">Editar categoria: <span style="color: #000">{{ $category->titulo }}</span></h5>
          
            <form method="POST" action="{{ route('category.update', $category) }}">
                @csrf @method('PATCH')
                <div class="mb-3">
                    <label for="titulo" class="form-label">Título:</label>
                    <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Escribe aqui el título de la categoria" value="{{ old('titulo', $category->titulo) }}">
                    @error('titulo')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>     
                <button type="submit" class="btn btn-success">Editar categoria</button>
            </form>
                  
        </div>
      </div>

      
    

</div>
@endsection
