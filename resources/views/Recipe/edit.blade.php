@extends('layouts.app')

@section('content')
<div class="backoffice-div">
    
    <div class="link_back">
        <a href="{{ route('recipe.index', 'page='.$page) }}"><i class="fas fa-chevron-left"></i> volver al listado de recetas</a>
    </div>
    <div class="card card_formulario" >
        <div class="card-body">
          <h5 class="card-title">Editar receta: <span style="color: #000">{{ $recipe->titulo }}</span></h5>
          
            <form method="POST" enctype="multipart/form-data" action="{{ route('recipe.update', [$recipe, $page]) }}">
                @csrf @method('PATCH')
                <div class="mb-3">
                    <label for="titulo" class="form-label">Título:</label>
                    <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Escribe aqui el título de la receta" value="{{ old('titulo', $recipe->titulo) }}">
                    @error('titulo')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="category_id" class="form-label">Selecciona una categoría:</label>
                    <select name="category_id" id="category_id" class="form-control">
                        <option>Ninguna categoría seleccionada</option>
                        @foreach ($categories as $id => $category_titulo)
                            <option 
                                value="{{ $id }}"
                                @if($id == old('category_id', $recipe->category_id)) selected @endif
                            >
                                {{ $category_titulo }}
                            </option> 
                        @endforeach                        
                    </select>
                    @error('category_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="row">
                     <div class="mb-3 col">
                        <label for="tiempo_preparacion" class="form-label">Tiempo de preparación (Minutos):</label>
                        <input type="number" class="form-control" id="tiempo_preparacion" name="tiempo_preparacion" placeholder="Escribe aqui el tiempo de preparación en minutos" value="{{ old('tiempo_preparacion', $recipe->tiempo_preparacion) }}">
                        @error('tiempo_preparacion')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>     
                    <div class="mb-3 col">
                        <label for="numero_raciones" class="form-label">Número de raciones:</label>
                        <input type="number" min="1" max="20" class="form-control" id="numero_raciones" name="numero_raciones" placeholder="Escribe aqui el número de raciones" value="{{ old('numero_raciones', $recipe->numero_raciones) }}">
                        @error('numero_raciones')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>   
                </div>  
                <div class="mb-3">
                    <label for="ingredientes" class="form-label">Ingredientes:</label>
                    <textarea type="text" class="form-control" id="ingredientes" name="ingredientes" placeholder="Escribe aqui los ingredientes">{{ old('ingredientes', $recipe->ingredientes) }}</textarea> 
                    @error('ingredientes')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="procedimiento" class="form-label">Procedimiento:</label>
                    <textarea type="text" class="form-control" id="procedimiento" name="procedimiento" placeholder="Escribe aqui el procedimiento">{{ old('procedimiento', $recipe->procedimiento) }}</textarea> 
                    @error('procedimiento')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <img src="/storage/{{ $recipe->imagen }}" alt="Imagen Receta" />
                    <label for="imagen" class="form-label">Selecciona una imagen</label>
                    <input class="form-control" type="file" id="imagen" name="imagen">
                    @error('imagen')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>  
                <button type="submit" class="btn btn-success">Editar receta</button>
            </form>
                  
        </div>
      </div>
</div>
@endsection