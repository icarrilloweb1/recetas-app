@extends('layouts.app')

@section('content')
<div class="backoffice-div">
    
    <div class="row">
        <h3 align="center">Listado de Recetas</h3>
        <div>
            <a href="{{ route('recipe.create', $recipes->currentPage()) }}" type="button" class="btn btn-success">Añadir nueva receta</a>
        </div>
        <div>
            {!! $recipes->withQueryString()->links('pagination::bootstrap-5') !!}
        </div>
        <div class="table-responsive">
            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center">#Id</th>
                        <th class="text-center">Título</th>
                        <th class="text-center">Categoría</th>
                        <th class="text-center">Imagen</th>
                        <th class="text-center">F. Publicación</th>
                        <th class="text-center">Activa</th>
                        <th class="text-center">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($recipes as $recipe)
                        <tr>
                            <td class="text-center">#{{ $recipe->id }}</td>
                            <td class="text-center">{{ $recipe->titulo }}</td>
                            <td class="text-center">{{ $recipe->category->titulo }}</td>
                            <td class="text-center"><img class="thumbail_backoffice" src="/storage/{{ $recipe->imagen }}" alt="Imagen Receta"></td>
                            <td class="text-center">{{ \Carbon\Carbon::parse($recipe->fecha_publicacion)->format('d-m-Y H:i:s') }}</td>
                            <td class="text-center">{{ $recipe->active ? 'SI' : 'NO' }}</td>
                            
                            <td class="text-center">
                                <a href="{{ route('recipe.edit', [$recipe, $recipes->currentPage()]) }}" type="button" class="btn btn-info">Editar</a>

                                <form method="POST" action="{{ route('recipe.active', [$recipe, $recipes->currentPage()]) }}" class="form_delete">
                                    @csrf @method('PATCH')   
                                    @if ($recipe->active)
                                        <button type="submit" class="btn btn-warning">Desactivar</button>
                                    @else
                                        <button type="submit" class="btn btn-success">Activar</button>
                                    @endif 
                                </form>
                                                                
                                <form method="POST" action="{{ route('recipe.destroy', [$recipe, $recipes->currentPage()]) }}" class="form_delete">
                                    @csrf @method('DELETE')    
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('¿Esta seguro de eliminar esta receta?')">Eliminar</button>
                                </form>    
                            </td>
                        </tr>  
                    @endforeach          
                </tbody>
            </table>
        </div>  
    </div>   
    <div>
        {!! $recipes->withQueryString()->links('pagination::bootstrap-5') !!}
    </div>

</div>
@endsection
