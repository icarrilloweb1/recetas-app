<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} | Recetas de Cocina</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
    <div id="app">  
        <header class="d-flex mb-3">
            <div class="me-auto p-2">
               <h1><a href="{{ route('home') }}">Recetas APP</a></h1>
            </div>
            <div class="p-2">
                <a href="{{ route("admin") }}" type="button" class="btn btn-light">Aceeder Admin</a>
            </div>               
        </header>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
</html>
