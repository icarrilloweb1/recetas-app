@extends('layouts.app')

@section('content')
<div class="backoffice-div">
    <div class="px-4 pt-5 my-5 text-center border-bottom">
        <h1 class="display-4 fw-bold">Bienvenido al BackOffice de RecetasApp</h1>
        <div class="col-lg-6 mx-auto">
          <p class="lead mb-4">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at turpis nec felis vestibulum consectetur vitae sed lacus. Vivamus gravida iaculis libero, sed pulvinar ante placerat id. Sed dapibus purus at nibh congue, et sagittis risus pulvinar. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec condimentum purus lacus.
          </p>
          <div class="d-grid gap-2 d-sm-flex justify-content-sm-center mb-5">
            <a href="{{ route('category.index')}}" class="btn btn-primary btn-lg px-4 me-sm-3">Gestionar Categorias</a>
            <a href="{{ route('recipe.index')}}" class="btn btn-primary btn-lg px-4">Gestionar Recetas</a>
          </div>
        </div>        
      </div>    
</div>
@endsection



