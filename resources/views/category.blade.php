@extends('layouts.homelayout')

@section('content')
<div class="container">

    <div class="titulo_categoria">
      <h3>Categoria: <b>{{ $category->titulo }}</b></h3>
      <a href="{{ route('home') }}"><i class="fas fa-chevron-left"></i> volver al Home</a>
    </div>
    
    <div class="contenedor_categorias">
      @foreach ($category->recipes->sortByDesc('fecha_publicacion') as $recipe)  
        <div class="categoria_item">
            <a href="/recipe/{{ $recipe->slug }}">
              <img src="/storage/{{ $recipe->imagen }}" alt="Imagen Receta" />
              <span class="recetas-titulo">{{ $recipe->titulo}}<br><small>{{ \Carbon\Carbon::parse($recipe->fecha_publicacion)->format('d-m-Y') }}</small></span>
            </a>
        </div>
      @endforeach
    </div>
    
	
</div>
@endsection