# Prueba Laravel - Salusplay - Biblioteca de recetas


## Comenzando 🚀

Todos los requisitos mínimos de la prueba se han cumplido. Con mas tiempo para la realización de este proyecto, se hubieran tomado decisiones distintas para mejorar el rendimiento y el diseño tanto del Frontend, como del Backed.


## Pre-requisitos 📋

En la configuración de las variables de entorno podemos ver como he utilizado MySQL, y en el caso concreto de mi servidor local con Xampp.

La Base de datos esta configurada con el nombre: <b>salusplay_recetas</b>, y en mi caso con el usuario root y sin contraseña.

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=salusplay_recetas
DB_USERNAME=root
DB_PASSWORD=
```

Por mi configuración con Xampp he tenido que cambiar tambien la url de la app. Es posible que no haya que sustituirla APP_URL por la proporcionada por su servidor local.

```
#APP_URL=http://localhost
APP_URL=http://127.0.0.1:8000
```

Las migraciones que vamos a ejecutar en la instalación, lanzaran a partir del BuildSeeder los distintos factories para User, Category y Recipe. Concretamente creamos con Faker, <b>1 Usuario (Admin / 12345), 5 Categorias y 100 Recetas</b>.

En "storage\app\public\images" he almacenado <b>10 imagenes por defecto</b> para la carga de datos iniciales de la app, que se asocian aleatoriamente a cada receta.

Estas imagenes no se eliminarán cuando eliminemos una receta de la app. Sin embargo, las imagenes que subamos desde el backoffice, si serán eliminadas del storage y por tanto de su link con la carpeta pública del proyecto.

Para que funcione correctamente la configuración del storage:link debemos configurar:

```
FILESYSTEM_DISK=public
```

Para la redimensión, al guardar y editar las imagenes, necesitaremos configurar en php.ini las siguientes extensiones:

```
extension=fileinfo
extension=gd
```


## Instalación 🔧

Realizar un checkout del Git.

```
git clone https://gitlab.com/icarrilloweb1/recetas-app.git
```

Instalamos modulos y dependencias

```
npm install 
```
```
composer install       
```

Ejecutamos el siguiente comando para generar un link publico a la carpeta storage y poder usar publicamente las imagenes que almacenamos desde el backoffice.  

```
php artisan storage:link
```

Ejecutamos las migraciones

```
php artisan migrate:fresh –seed
```

Ejecutamos los siguientes comandos.

```
npm run dev
```

```
php artisan serve
```

La consola nos indicará que ruta debemos usar para ver el proyecto en desarrollo.
En mi caso concreto, con mi configuración: <b>http://127.0.0.1:8000</b>


## Consideraciones 🛠️

Para la visualización del front, he utilizado unicamente css y javascript vanilla, debido a que con pocas lineas de código conseguimos la funcionalidad deseada con un redimiento optimo. Valoré crear un componente de Vue pero por practicidad y rapidez deseche la idea.

Si eliminamos una categoria automaticamente se eliminarán todas las recetas con esa categoria asignada.

Si desactivamos una categoria, no he cambiado el campo "active" de las recetas asociadas a 0, ya que en el front solo se mostrarán los carrouseles de recetas de las categorias activas.

En la parte de backoffice los listados de categorias y recetas son distintos. Uno lo he realizado unicamente con maquetación con bootstrap a falta de darle funcionalidad a la ordenación de las celdas y su filtrado directo con javascript y el otro listado he utilizado Laravel DataTables con jquery y algun script para enviar solicitudes con ajax, simplemente por mostrar distintas opciones de listados grid.

<b>Espero haber cumplido con todos los requisitos requeridos en el documento proporcionado para la prueba.</b>

Muchas gracias por todo.


## Autor ✒️

Israel Carrillo | 676 435 719 | icarrilloweb@gmail.com
<br><br><br>


