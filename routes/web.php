<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\RecipeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//rutas generadas para todo el sistema de gestión de los usuarios
Auth::routes();

//rutas públicas Front
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/category/{category}', [HomeController::class, 'showCategory'])->name('showCategory');
Route::get('/recipe/{recipe}', [HomeController::class, 'showRecipe'])->name('showRecipe');

//rutas con acceso administrador BackOffice
Route::get('/admin', [AdminController::class, 'index'])->name('admin');

Route::get('/admin/categories', [CategoryController::class, 'index'])->name('category.index');
Route::get('/admin/categories/crear', [CategoryController::class, 'create'])->name('category.create');
Route::post('/admin/categories', [CategoryController::class, 'store'])->name('category.store');
Route::get('/admin/categories/{slug}/editar', [CategoryController::class, 'edit'])->name('category.edit');
Route::patch('/admin/categories/{category}', [CategoryController::class, 'update'])->name('category.update');
Route::get('/admin/categories/{id}/active', [CategoryController::class, 'active'])->name('category.active');
Route::get('/admin/categories/delete/{category}', [CategoryController::class, 'destroy'])->name('category.destroy');

Route::get('/admin/recipes', [RecipeController::class, 'index'])->name('recipe.index');
Route::get('/admin/recipes/crear/{page}', [RecipeController::class, 'create'])->name('recipe.create');
Route::post('/admin/recipes/{page}', [RecipeController::class, 'store'])->name('recipe.store');
Route::get('/admin/recipes/{recipe}/{page}/editar', [RecipeController::class, 'edit'])->name('recipe.edit');
Route::patch('/admin/recipes/{recipe}/{page}', [RecipeController::class, 'update'])->name('recipe.update');
Route::patch('/admin/recipes/{recipe}/{page}/active', [RecipeController::class, 'active'])->name('recipe.active');
Route::delete('/admin/recipes/{recipe}/{page}', [RecipeController::class, 'destroy'])->name('recipe.destroy');