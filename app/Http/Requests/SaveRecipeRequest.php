<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SaveRecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //aqui podriamos autorizar a un tipo concreto de usuario
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'titulo' => [
                'required',
                'max:255',
                Rule::unique('recipes')->ignore($this->route('recipe'))
            ],
            'tiempo_preparacion' => [
                'required'
            ],
            'numero_raciones' => [
                'required'
            ],
            'ingredientes' => [
                'required'
            ],
            'procedimiento' => [
                'required'
            ],
            'category_id' => [
                'required',
                'integer'
            ],
            'imagen' => [
                $this->route('recipe') ? 'nullable' : 'required',
                'mimes:jpg,jpeg,png'
            ]
        ];
    }
}
