<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Models\Recipe;
use App\Models\Category;
use DataTables;     

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {   
        //solo retornamos la vista a modo de introduccion al backoffice
        return view('admin');
    }
}
