<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\SaveCategoryRequest;
use App\Models\Category;
use DataTables; 

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {   
        //En el listado de gestión de las categorias he usado Laravel DataTables 7.0 para mostrar dos formas distintas de grids y 
        //necesitamos devolver los datos en json atraves de ajax
        if ($request->ajax()) {
            $category = Category::select('id','titulo','slug', 'active')->get();
            return Datatables::of($category)->addIndexColumn()
                ->addColumn('action', function($category){
                    $button = '<button type="button" name="edit" slug="'.$category->slug.'" class="edit btn btn-info">Editar</button>';
                    $button .= '<button type="button" name="activebtn" id="'.$category->id.'" class="activebtn btn btn-warning">Activar/Desactivar</button>';
                    $button .= '<button type="button" name="delete" id="'.$category->id.'" class="delete btn btn-danger" textoconfirm="¿Esta segur@ de eliminar este registro?">Eliminar</button>';
                    return $button;
                })
                ->make(true);
        } 
        return view('category.index');
    }

    public function create()
    {   
        return view('Category.create');
    }

    public function store(SaveCategoryRequest $request) //Utilizamos el mismo Request SaveCategoryRequest para Autorizar y validar los cambios en el modelo
    {   
        $fields = $request->validated();
        $fields['slug'] = Str::slug($fields['titulo']);        
        Category::create($fields);
        return redirect()->route('category.index');
    }

    public function edit($slug)
    {   
        $category = Category::where('slug', $slug)->firstOrFail();
        return view('Category.edit', [
            'category' => $category
        ]);
    }

    public function update(Category $category, SaveCategoryRequest $request)
    {   
        $fields = $request->validated();
        $fields['slug'] = Str::slug($fields['titulo']);   
        $category->update($fields);
        return redirect()->route('category.index');
    }
    
    public function destroy($id)
    {   
        $category = Category::findOrFail($id);
        $category->delete();
    }
    
    public function active($id)
    {   
        $category = Category::findOrFail($id);
        $fields['active'] = $category->active === 1 ? 0 : 1;
        $category->update($fields);        
    }

    

    
}
