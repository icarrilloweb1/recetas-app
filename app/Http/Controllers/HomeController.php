<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Recipe;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //En este controlador he concentrado todas las vistas del Front por simplicidad
    //En el caso de ser un proyecto mas grande se podria organizar por Controladores independientes

    public function index()
    {
        return view('home', [
            'categories' => Category::with([
                'recipes' => function ($q){
                    $q->where('active', 1)->orderBy('fecha_publicacion', 'DESC'); //primero mostramos las recetas mas actuales
                }
                ])->where('active', 1)->get()->sortBy('titulo') //ordenamos las categorias por orden alfabetico del título
        ]);
    }

    public function showCategory(Category $category)
    {
        return view('category', [
            'category' => $category
        ]);
    }


    public function showRecipe(Recipe $recipe)
    {
        return view('recipe', [
            'recipe' => $recipe
        ]);
    }
}
