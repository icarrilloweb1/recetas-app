<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Models\Recipe;
use App\Http\Requests\SaveRecipeRequest;
use App\Models\Category;
use App\Events\RecipeSaved;

class RecipeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //Listado para grid paginado manualmente.
    //Quedaría pendiente toda la funcionalidad de ordenación y filtrado por texto en javascript

    public function index()
    {   
        return view('recipe.index', [
            'recipes' => Recipe::with('category')->orderBy('id', 'DESC')->paginate(5)
        ]);        
    }  

    public function create($page = 1)
    {   
        return view('Recipe.create', [
            'categories' => Category::pluck('titulo', 'id'), 
            'page' => $page
        ]);
    }

    public function store($page = 1, SaveRecipeRequest $request) //Utilizamos el mismo Request SaveRecipeRequest para Autorizar y validar los cambios en el modelo
    {   
        $fields = $request->validated();
        $fields['slug'] = Str::slug($fields['titulo']);        
        $fields['fecha_publicacion'] = date('Y-m-d H:i:s');
        
        $recipe = new Recipe($fields);
        $recipe->imagen = $request->file('imagen')->store('images');
        $recipe->save();

        //Funcion para la redimension de la imagen **
        $image = Image::make(Storage::get($recipe->imagen))
                 ->widen(800) //800px de ancho sin perder el ratio
                 ->limitColors(255) //reducimos los colores para reducir peso
                 ->encode(); //codificamos a la extension correcta de la imagen
        Storage::put($recipe->imagen, (string) $image);
        //Funcion para la redimension de la imagen
        
        // ** Toda la funcion de la redimension de la imagen la queria delegar a un listener...pero no se porque motivo no lo lanza automaticamente.
        //Con mas tiempo depuraria hasta encontrar el error para que el usuario no tenga que esperar a que se optimice la imagen
        //RecipeSaved::dispatch($recipe); //lanzamos el evento que pasara $recipe al listener para optimizar la imagen

        return redirect()->route('recipe.index', 'page='.$page);
    }

    public function edit(Recipe $recipe, $page = 1)
    {   
        return view('Recipe.edit', [
            'recipe' => $recipe,
            'categories' => Category::pluck('titulo', 'id'), 
            'page' => $page
        ]);
    }

    public function update(Recipe $recipe, $page = 1, SaveRecipeRequest $request)
    {           
        $fields = $request->validated();
        $fields['slug'] = Str::slug($fields['titulo']); 
        
        if ($request->hasFile('imagen')) {
            
            //borramos la imagen actual sino es una imagen por defecto del sistema
            if(!str_contains($recipe->imagen, "image_default")) {
                Storage::delete($recipe->imagen); 
            }            

            $recipe->fill($fields);
            $recipe->imagen = $request->file('imagen')->store('images');
            $recipe->save(); //editamos el registro ya con la info de la nueva imagen

            //redimensionamos la imagen para que no pese demasido y ahorrar en espacio
            $image = Image::make(Storage::get($recipe->imagen))->widen(800)->limitColors(255)->encode();
            Storage::put($recipe->imagen, (string) $image);

        } else {
            $recipe->update(array_filter($fields));
        }
        
        return redirect()->route('recipe.index', 'page='.$page);
    }

    public function destroy(Recipe $recipe, $page = 1)
    {           
        if(!str_contains($recipe->imagen, "image_default")) {
            Storage::delete($recipe->imagen);
        } 
        $recipe->delete();
        return redirect()->route('recipe.index', 'page='.$page);
    }

    public function active(Recipe $recipe, $page = 1)
    {   
        $fields['active'] = $recipe->active === 1 ? 0 : 1;
        $recipe->update($fields);
        return redirect()->route('recipe.index', 'page='.$page);
    }

}
