<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    use HasFactory;

    protected $fillable = ['titulo', 'slug', 'active', 'category_id', 'imagen', 'tiempo_preparacion', 'numero_raciones', 'ingredientes', 'procedimiento', 'fecha_publicacion'];
    
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
