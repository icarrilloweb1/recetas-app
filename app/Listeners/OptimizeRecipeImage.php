<?php

namespace App\Listeners;

use App\Events\RecipeSaved;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class OptimizeRecipeImage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \app\Events\RecipeSaved  $event
     * @return void
     */
    public function handle(RecipeSaved $event)
    {
        $image = Image::make(Storage::get($event->$recipe->imagen))
                 ->widen(800)
                 ->limitColors(255)
                 ->encode();

        Storage::put($event->$recipe->imagen, (string) $image);
    }
}
