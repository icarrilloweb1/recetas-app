<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class RecipeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $title = fake()->sentence(5, false);
        return [
            'category_id' => random_int(1, 5),
            'titulo' => $title,
            'slug' => Str::slug($title),
            'imagen' => 'images/image_default_'.random_int(1, 10).'.jpg',
            'tiempo_preparacion' => random_int(10, 180),
            'numero_raciones' => random_int(2, 8),
            'ingredientes' => fake()->paragraph(5, true),
            'procedimiento' => fake()->text(),
            'fecha_publicacion' => fake()->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = 'UTC')
        ];
        
    }
}
