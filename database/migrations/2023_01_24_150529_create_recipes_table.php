<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->id(); 
            $table->unsignedBigInteger('category_id');        
            $table->string('titulo', 255)->unique();
            $table->string('slug', 255)->unique();
            $table->string('imagen')->nullable();
            $table->string('tiempo_preparacion');
            $table->unsignedInteger('numero_raciones');
            $table->text('ingredientes')->nullable();
            $table->text('procedimiento')->nullable();            
            $table->timestamp('fecha_publicacion');   
            $table->boolean('active')->default(true);   
            $table->timestamps();       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
};
